package fr.gcrampe.mobilemocn;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.IBinder;
import android.preference.PreferenceManager;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.ProxySelector;
import java.util.ArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/**
 * Created by Boudat David on 25/03/15.
 * Service s'occupant de l'obtention de la liste des MOCN depuis le WebService.
 */

public class AgentWs extends Service {

    private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
    //Methode executé dans le thread
    private final Runnable majFlux = new Runnable() {
        public void run() {
            sendMocn();
        }
    };
    private ScheduledFuture thread;
    //Listener sur changement de configuration
    private SharedPreferences.OnSharedPreferenceChangeListener prefListener = new SharedPreferences.OnSharedPreferenceChangeListener() {
        public void onSharedPreferenceChanged(SharedPreferences prefs, String key) {
            if (key.equals(SettingsActivity.FREQUENCE_MAJ) || key.equals(SettingsActivity.ADRESSE)) {
                thread.cancel(true);
                thread = scheduler.scheduleAtFixedRate(majFlux, 0, Integer.parseInt(prefs.getString("Frequence_Maj", null)), TimeUnit.SECONDS);
                System.out.println(thread.getDelay(TimeUnit.SECONDS));
            }
        }
    };
    private SharedPreferences prefs;
    private RefreshReceiver receiver;

    /**
     * Retourne un lien de communication avec le service lors de l'appel de bindService.
     *
     * @param intent Intent à l'origine de la création du service.
     * @return null car on n'a pas besoin de lien de communication avec ce service.
     */
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /**
     * Méthode appelée lors de la création du service
     */
    @Override
    public void onCreate() {
        // Obtention du fichier de configuration
        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        // Ajout d'un listener sur les changements de configuration
        prefs.registerOnSharedPreferenceChangeListener(prefListener);
        // Lancement du Thread de mise à jour cyclique selon la configuration sauvegardée
        thread = scheduler.scheduleAtFixedRate(majFlux, 0, Integer.parseInt(prefs.getString("Frequence_Maj", null)), TimeUnit.SECONDS);
        // Création d'un receveur d'intent pour les demandes de rafraichissement
        registerReceiver(new RefreshReceiver(), new IntentFilter("Refresh"));
    }

    /**
     * Methode appelée après la création du service par un appel par startservice
     *
     * @param intent  Intent à l'origine de la création du service.
     * @param flags   Donnée supplementaire.
     * @param startId Un int représentant la requête de demarrage.
     * @return START_STICKY le service est détruit s'il n'est plus utilisé.
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        return START_STICKY;
    }

    /**
     * Appelée à la destruction du service
     */
    @Override
    public void onDestroy() {
        unregisterReceiver(receiver);
        thread.cancel(true); // Arret du thread de mise à jour cyclique
        super.onDestroy();
    }

    /**
     * Méthode récupérant la liste de MOCN depuis le service sous la forme d'une arraylist d'objet
     */
    private void sendMocn() {
        Intent intention = new Intent("MAJ_DATA");
        String Adresse;
        boolean Success = true;
        Adresse = prefs.getString("Adresse", null); // Recuperation de l'adresse du webservice
        try {
            String Json = wget(Adresse);
            intention.putExtra("Flux", jsonParseToMOCN(Json)); // Ajout l'arraylist dans l'Intent
        } catch (JSONException ex) {
            //En cas d'exception on ajoute le message d'erreur et on passe success sur false
            Success = false;
            intention.putExtra("Exception", "Erreur lors de la lecture JSON : " + ex.getMessage());
            ex.printStackTrace();
        } catch (IOException ex) {
            //En cas d'exception on ajoute le message d'erreur et on passe success sur false
            Success = false;
            intention.putExtra("Exception", "Erreur lors de l'accès au service web : " + ex.getMessage());
            ex.printStackTrace();
        }
        intention.putExtra("Success", Success);
        sendBroadcast(intention);
    }

    /**
     * Récupère le flux JSON sous forme de texte
     *
     * @param url Adresse du webservice
     * @return Chaine de caractere du flux JSON
     * @throws IOException
     */
    private String wget(String url) throws IOException {

        ProxySelector.setDefault(null); //On ignore le proxy
        HttpParams httpParameters = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(httpParameters, 15000); //Reglage du timeout sur 15 secondes
        HttpClient client = new DefaultHttpClient(httpParameters);
        HttpGet httpGet = new HttpGet(url); //On cree une requete sur l'adresse contenu dans la chaine de caractere url
        HttpResponse httpResponse = client.execute(httpGet);
        HttpEntity httpEntity = httpResponse.getEntity();

        if (httpEntity != null) {
            InputStream inputStream = httpEntity.getContent();

            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(inputStream));
            StringBuilder stringBuilder = new StringBuilder();

            String ligneLue = bufferedReader.readLine(); //Lecture d'une ligne de la page
            while (ligneLue != null) {
                stringBuilder.append(ligneLue).append("\n"); //On insere la ligne lue dans une chaine de caractere
                ligneLue = bufferedReader.readLine();
            }
            bufferedReader.close(); //Arret de la lecture
            return stringBuilder.toString(); //On retourne la chaine de caractere
        }
        return null;
    }

    /**
     * Transforme une chaine de caractere en une ArrayList d'objet MOCN
     *
     * @param JSON Chaine de caractere du flux JSON
     * @return liste de MOCN
     * @throws JSONException
     */
    private ArrayList<MOCN> jsonParseToMOCN(String JSON) throws JSONException {
        ArrayList<MOCN> ArrayMocn = new ArrayList<>();
        JSONArray jsonArray = new JSONArray(JSON); // Construction d'un JSONArray à partir d'une chaine de caractere
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            //Ajout d'un objet MOCN
            ArrayMocn.add(
                    //Creation d'un objet MOCN
                    new MOCN(
                            jsonObject.getInt("id"),
                            jsonObject.getInt("etat"),
                            jsonObject.getInt("octet"),
                            jsonObject.getString("date"),
                            jsonObject.getString("nom"),
                            jsonObject.getString("ip")
                    )
            );
        }
        return ArrayMocn;
    }

    // Classe interne contenant une tache s'executant en arriere plan
    private class ActionRefresh extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            sendMocn();
            return null;
        }

    }

    // Envoie d'un intent lors d'un demande de raffraichissement
    private class RefreshReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            new ActionRefresh().execute();
        }
    }
}
