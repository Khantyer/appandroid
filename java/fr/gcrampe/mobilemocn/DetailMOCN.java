package fr.gcrampe.mobilemocn;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;


public class DetailMOCN extends ActionBarActivity {

    private TextView etat_txt, octet, ip, date, nom;
    private ImageView etat_img;
    private int idMOCN;
    private Context contexte;
    private DetailMajReceiver receiver;
    private Boolean fenetreErreurShown;


    //Méthode appelée lors de la création du service

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        fenetreErreurShown = false;
        contexte = this;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_mocn);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        etat_txt = (TextView) findViewById(R.id.detail_textView_etat_texte);
        octet = (TextView) findViewById(R.id.detail_textView_octet);
        ip = (TextView) findViewById(R.id.detail_textView_ip);
        date = (TextView) findViewById(R.id.detail_textView_date);
        nom = (TextView) findViewById(R.id.detail_textView_nom);
        etat_img = (ImageView) findViewById(R.id.detail_etat_img);

        miseEnPage((MOCN) getIntent().getParcelableExtra("MOCN")); //Obtention de la mocn passée en Intent

        //Ecoute des intent de maj des données
        IntentFilter filter = new IntentFilter("MAJ_DATA");
        receiver = new DetailMajReceiver();
        registerReceiver(receiver, filter);
    }

    /***
     * Réalise l'affichage d'une mocn dans une activity
     *
     * @param mocn L'objet MOCN a afficher en détail
     */
    private void miseEnPage(MOCN mocn) {
        idMOCN = mocn.getID();

        ip.setText(mocn.getIP());
        date.setText(mocn.getDate());
        nom.setText(mocn.getNom());
        octet.setText(Integer.toString(mocn.getOctet()));

        int idImage;
        String etat;

        switch (mocn.getEtat()) {
            case -1:
                idImage = R.drawable.ic_lg_disconnected;
                etat = "Deconnectée";
                break;

            case 0:
                idImage = R.drawable.ic_lg_stopped;
                etat = "Arrêtée";
                break;

            default:
                idImage = R.drawable.ic_lg_problem;
                etat = "Production dégradée";
                break;

            case 2:
                idImage = R.drawable.ic_lg_ok;
                etat = "Production normale";
                break;
        }

        etat_txt.setText(etat + " (" + Integer.toString(mocn.getEtat()) + ")");
        etat_img.setImageResource(idImage);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    /***
     * Se declenche lors d'une action sur un MenuItem du menu
     * @param item Le MenuItem actionné
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            startActivity(new Intent(this, SettingsActivity.class));
            return true;
        }

        if (id == R.id.action_refresh) {
            sendBroadcast(new Intent("Refresh"));
        }

        return super.onOptionsItemSelected(item);
    }

    //Classe interne décrivant les actions réalisé lors de la reception d'une trame
    public class DetailMajReceiver extends BroadcastReceiver {

        /***
         * Methode execute lors de la reception d'un intent
         *
         * @param context Contexte de l'activité
         * @param intent Intention recu
         */
        @Override
        public void onReceive(final Context context, Intent intent) {
            if (intent.getBooleanExtra("Success", false)) { //Verification de la reussite de la mise a jour
                recuperationMOCN(intent); //Recuperation de la mocn puis mise en page
            } else {
                Erreur(intent); //Affichage de l'erreur rencontree
            }
        }

        private void recuperationMOCN(Intent intention) {
            int numMOCN = 0;
            ArrayList<MOCN> mocn = intention.getParcelableArrayListExtra("Flux"); //Recuperation de l'ArrayList de MOCN
            try {
                while (mocn.get(numMOCN).getID() != idMOCN) { //On tente de recuperer la MOCN actuellement affiché declenche une
                    numMOCN++;                                //NullPointerException si introuvable
                }
            } catch (NullPointerException ex) {
                if (!fenetreErreurShown) { //Verifie la presence d'une fenetre pour eviter l'empilement
                    fenetreErreurShown = true; //On indique la presence de la fenetre
                    new AlertDialog.Builder(contexte) //Fabrication d'une fenetre
                            .setTitle("Erreur")
                            .setMessage("La MOCN demandée n'existe plus")
                            .setNeutralButton("Ok", new DialogInterface.OnClickListener() {
                                //Action lors de l'appui sur OK
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                    fenetreErreurShown = false;
                                    startActivity(new Intent(contexte, ListeMocn.class));
                                }
                            })
                            .setIconAttribute(android.R.attr.alertDialogIcon)
                            .show();
                }
            }
            miseEnPage(mocn.get(numMOCN));
        }

        private void Erreur(Intent intention) {
            if (!fenetreErreurShown) {
                fenetreErreurShown = true;
                new AlertDialog.Builder(contexte)
                        .setTitle("Erreur")
                        .setMessage(intention.getStringExtra("Exception")) //Message d'erreur
                        .setNeutralButton(R.string.title_activity_settings, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                                fenetreErreurShown = false;
                                startActivity(new Intent(contexte, SettingsActivity.class));
                            }
                        })
                        .setIconAttribute(android.R.attr.alertDialogIcon)
                        .show();
            }
        }
    }

    public void onDestroy() {
        unregisterReceiver(receiver);
        super.onDestroy();
    }
}
