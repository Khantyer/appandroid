package fr.gcrampe.mobilemocn;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Boudat David on 25/03/15.
 * Mise en page de la liste des mocn dans la ListView
 */

class CustomAdapter extends ArrayAdapter<MOCN> {

    private final Context context;
    private final ArrayList<MOCN> values;

    public CustomAdapter(Context context, ArrayList<MOCN> objects) {
        super(context, R.layout.lignelist, objects);
        this.context = context;
        this.values = objects;
    }

    /***
     * Genere un element de la listview
     *
     * @param position Position de la MOCN dans la liste
     * @return Un fragment de la ListView
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rView = inflater.inflate(R.layout.lignelist, parent, false);

        TextView nom = (TextView) rView.findViewById(R.id.Lignelist_Nom);
        TextView date = (TextView) rView.findViewById(R.id.Lignelist_Date);
        ImageView etat = (ImageView) rView.findViewById(R.id.Lignelist_Etat);

        nom.setText("Nom : " + values.get(position).getNom());
        date.setText("Date : " + values.get(position).getDate());

        int idImage = 0;
        //Selection de l'icone en fonction de l'etat
        switch (values.get(position).getEtat()) {
            case -1:
                idImage = R.drawable.ic_sm_disconnected;
                break;

            case 0:
                idImage = R.drawable.ic_sm_stopped;
                break;

            case 1:
                idImage = R.drawable.ic_sm_problem;
                break;

            case 2:
                idImage = R.drawable.ic_sm_ok;
                break;
        }

        etat.setImageResource(idImage);

        return rView;
    }
}
