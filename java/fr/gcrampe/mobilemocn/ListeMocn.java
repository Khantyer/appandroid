package fr.gcrampe.mobilemocn;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;


public class ListeMocn extends ActionBarActivity {

    private ArrayList<MOCN> mocnArrayList;
    private ListView liste;
    private CustomAdapter adapter;
    private Context contexte;
    private ListeMajReceiver receiver;
    private boolean fenetreErreurShown = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        contexte = this;
        if (!isConnected()) {
            startActivity(new Intent(WifiManager.ACTION_PICK_WIFI_NETWORK));

        }
        setContentView(R.layout.activity_liste_mocn);
        liste = (ListView) findViewById(R.id.Liste);
        liste.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Intent Details = new Intent(contexte, DetailMOCN.class);
                Details.putExtra("MOCN", mocnArrayList.get(position));
                startActivity(Details);
            }
        });
        PreferenceManager.setDefaultValues(this, R.xml.pref_general, false);
        startService(new Intent(contexte, AgentWs.class));
        IntentFilter filter = new IntentFilter("MAJ_DATA");
        receiver = new ListeMajReceiver();
        registerReceiver(receiver, filter);
    }

    private boolean isConnected() {
        ConnectivityManager cm =
                (ConnectivityManager) contexte.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            startActivity(new Intent(contexte, SettingsActivity.class));
            return true;
        }

        if (id == R.id.action_refresh) {
            sendBroadcast(new Intent("Refresh"));
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDestroy() {
        unregisterReceiver(receiver);
        stopService(new Intent(contexte, AgentWs.class));
        super.onPause();
    }

    public class ListeMajReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getBooleanExtra("Success", false)) {
                mocnArrayList = intent.getParcelableArrayListExtra("Flux");
                if (adapter == null) {
                    adapter = new CustomAdapter(contexte, mocnArrayList);
                    liste.setAdapter(adapter);
                } else {
                    adapter.clear();
                    adapter.addAll(mocnArrayList);
                }
            } else {
                if (!fenetreErreurShown) {
                    fenetreErreurShown = true;
                    new AlertDialog.Builder(contexte)
                            .setTitle("Erreur")
                            .setMessage(intent.getStringExtra("Exception"))
                            .setNeutralButton(R.string.title_activity_settings, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                    fenetreErreurShown = false;
                                    startActivity(new Intent(contexte, SettingsActivity.class));
                                }
                            })
                            .setIconAttribute(android.R.attr.alertDialogIcon)
                            .show();
                }
            }
        }
    }

}
