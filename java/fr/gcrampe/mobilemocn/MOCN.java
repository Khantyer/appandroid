package fr.gcrampe.mobilemocn;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by david boudat on 25/03/15.
 */
public class MOCN implements Parcelable {

    private int ID, Etat, Octet;
    private String Date, Nom, IP;

    /***
     * Construction d'une MOCN par le passage de plusieur parametre
     * @param id id dans la base de donnée
     * @param etat Etat actuel de la MOCN
     * @param octet Octet representant le statut de la MOCN
     * @param date Date & heure de l'enregistrement
     * @param nom Nom de la MOCN
     * @param ip IP de la MOCN
     */
    public MOCN(int id, int etat, int octet, String date, String nom, String ip) {
        this.ID = id;
        this.Etat = etat;
        this.Octet = octet;
        this.Date = date;
        this.Nom = nom;
        this.IP = ip;
    }

    //Construction d'une MOCN par le passage d'un Parcel
    public MOCN(Parcel in) {
        this.ID = in.readInt();
        this.Etat = in.readInt();
        this.Octet = in.readInt();
        this.Date = in.readString();
        this.Nom = in.readString();
        this.IP = in.readString();
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getEtat() {
        return Etat;
    }

    public void setEtat(int etat) {
        Etat = etat;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getNom() {
        return Nom;
    }

    public void setNom(String nom) {
        Nom = nom;
    }

    public int getOctet() {
        return Octet;
    }

    public void setOctet(int octet) {
        Octet = octet;
    }

    public String getIP() {
        return IP;
    }

    public void setIP(String IP) {
        this.IP = IP;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    //Creation d'un Parcel à partir de chaque MOCN
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(ID);
        dest.writeInt(Etat);
        dest.writeInt(Octet);
        dest.writeString(Date);
        dest.writeString(Nom);
        dest.writeString(IP);
    }

    public static final Parcelable.Creator<MOCN> CREATOR = new Parcelable.Creator<MOCN>() {
        @Override
        public MOCN createFromParcel(Parcel source) {
            return new MOCN(source);
        }

        @Override
        public MOCN[] newArray(int size) {
            return new MOCN[size];
        }
    };
}
